import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import { PanelHeader } from "components";

import Select from "react-select";

import Button from "components/CustomButton/CustomButton.jsx";



class Intents extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      error: null,
      isLoaded: false,
      IntentList:[{value: "General_Greetings", label: "General_Greetings"},{value: "General_Greetings2", label: "General_Greetings2"}],
      IntentScores:[],
      IntentMessages:[],
      data:[],
      Intent: "General_Greetings",
      Score: {min:0, max:0, mean:0, median:0}
    };
  }
  
  /*componentDidMount() {
    fetch('http://localhost:3999/intentscores?intent=General_Greetings')
    .then(response => response.json())
    .then(
      (result) => {
        this.setState({
          isLoaded:true,
          IntentScores:result.sort((a, b) => a - b),
          data:[]
        })
        this.setState({
          Score:{min:this.state.IntentScores[0], max:this.state.IntentScores[this.state.IntentScores.length-1]}
        })
      }, 
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }*/
  
  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Overall Stats</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Col xs={12} md={6}>
                      <CardTitle tag="h4">Select Intent</CardTitle>
                      <Row>
                        <Col xs={12} md={6}>
                          <Select
                            className="react-select primary"
                            classNamePrefix="react-select"
                            placeholder={this.state.Intent}
                            name="singleSelect"
                            value={this.state.Intent}
                            options={this.state.IntentList}
                            onChange={value =>
                              this.setState({ Intent: value })
                            }
                          />
                        </Col>
                      </Row>
                    </Col>
                    
                    </Row>
                    
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Intents;
