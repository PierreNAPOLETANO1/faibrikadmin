import React, { Component } from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

import {
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  CardFooter,
  Row,
  Col
} from "reactstrap";

import Switch from "react-bootstrap-switch";

import { PanelHeader } from "components";

import Button from "components/CustomButton/CustomButton.jsx";



class GeneralParams extends Component {

  render() {
    return (
      <div>
        <PanelHeader size="sm" />
        <div className="content">
          <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Customer Care Opening Hours</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Label sm={2}>Monday</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder="9h-13h,14h-18h"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Tuesday</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder="9h-13h,14h-18h" 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Wednesday</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input 
                          type="text" 
                          placeholder="9h-13h,14h-18h" 
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Label sm={2}>Thursday</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder="9h-13h,14h-18h"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                    <Label sm={2}>Friday</Label>
                    <Col xs={12} sm={10}>
                      <FormGroup>
                        <Input
                          type="text"
                          placeholder="9h-13h,14h-18h"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                  <Label sm={2}>Saturday</Label>
                  <Col xs={12} sm={10}>
                    <FormGroup>
                      <Input
                        type="text"
                        placeholder="9h-13h,14h-18h"
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                <Label sm={2}>Sunday</Label>
                <Col xs={12} sm={10}>
                  <FormGroup>
                    <Input
                      type="text"
                      placeholder="Closed"
                    />
                  </FormGroup>
                </Col>
              </Row>
                    <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button color="info" leftLabel="now-ui-icons ui-1_check" >
                      Save Opening Hours
                    </Button>
                    <Button color="success" leftLabel="now-ui-icons ui-1_lock-circle-open">
                      Open Customer Care
                    </Button>
                    <Button color="danger" leftLabel="now-ui-icons ui-1_simple-remove" >
                      Close Customer Care
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>

          <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">AI Sensitivity</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Label sm={2}>Confidence Level</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder="65%"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    
                    <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button color="info" leftLabel="now-ui-icons ui-1_check" >
                      Save
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>

 <Row>
          <Col md={12} xs={12}>
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Conversation Timeouts</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form className="form-horizontal">
                    <Row>
                    <Label sm={2}>Inactivity Timeout (min)</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder="5"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                    <Label sm={2}>Archiving Timeout (min)</Label>
                      <Col xs={12} sm={10}>
                        <FormGroup>
                          <Input
                            type="text"
                            placeholder="60"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
              <Row>
          <Col xs={12} md={6}>
                <CardBody>
                  <div className="btns-mr-5">
                    <Button color="info" leftLabel="now-ui-icons ui-1_check" >
                      Save
                    </Button>
                  </div>
                </CardBody>
              </Col>
            </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>

        </div>
      </div>
    );
  }
}
export default GeneralParams;
